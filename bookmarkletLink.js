var classes = []; // The output array
$("[class]").each(function(){ // This will log elements with an empty but present class attribute as well

	var classNames = $(this).attr("class").split(' '); // It's not clear how to handle elements with multiple classes, so I decided to log such elements multiple times, one per class name
	var pos = {'top': $(this).offset().top, 'left': $(this).offset().left, 'width': $(this).width(), 'height':  $(this).height()}; // The position info
	var found = false; // For flaging class names that already exist in the struct
	
	classNames.forEach( function(className) { // For each class name of the current element
		classes.forEach(function(entry, index){ // Loop through the struct
			if (entry.className == className) { // Does the class name exist?
				classes[index].elements.push(pos); // Append the new element info
				found = true; // Flag as present
			}
		});
		if (!found) { // If the class name doesn't exist, add it
			classes.push({ className: className, elements: [pos]});
		}
	});
	
});
console.log(classes);