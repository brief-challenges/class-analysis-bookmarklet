# Class analysis bookmarklet

This is a bookmarklet that can analyse a web page, list all of the classes and where they are positioned on the page.

The project contains an HTML page with the source of the JavaScript and a bookmarklet link that can be dragged up to the bookmarks bar.

[Bookmarkleter](http://chriszarate.github.io/bookmarkleter/) was used to generate the bookmarklet link.

### How it works

The tool works by:

 - finding every element on the page that has a class assigned to it.
 - measuring the position ('top', 'left') and the size in pixels of each of the elements.
 - generating a JavaScript array with the elements, grouped by class name
```
 [
  {
    className: "class1",
    elements: [
      {
        top: 0,
        left: 0,
        width: 100,
        height: 30
      }
      ...
    ]
  },
  {
    className: "class2",
    elements: [
      {
        top: 0,
        left: 0,
        width: 100,
        height: 30
      }
      ...
    ]
  },
  ...
 ]
```
 - using console.log to output the array


